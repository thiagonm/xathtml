<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
html {
    width: 100%;
    height: 100%;
  }
  body {
    color: rgba(0, 0, 0, 0.6);
    font-family: "helvetica", sans-serif;
    font-size: 14px;
    line-height: 1.6em;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  .formulari {
    z-index: 15;
    position: relative;
    background: #FFFFFF;
    width: 600px;
    border-radius: 9px;
    box-shadow: 0 0 30px rgba(0, 0, 0, 0.1);
    box-sizing: border-box;
    margin: 100px auto 10px;
    overflow: hidden;
  }
  .formulari__grup {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    margin: 0 0 20px;
  }

  .formulari__grup label {
    display: block;
    margin: 0 0 10px;
    color: rgba(0, 0, 0, 0.6);
    font-size: 12px;
    font-weight: 500;
    line-height: 1;
    text-transform: uppercase;
    letter-spacing: 0.2em;
  }
  .formulari__grup input {
    outline: none;
    display: block;
    background: rgba(0, 0, 0, 0.1);
    width: 100%;
    border: 0;
    border-radius: 6px;
    padding: 10px 20px;
    color: rgba(0, 0, 0, 0.6);
    font-family: Arial;
    font-size: inherit;
    font-weight: 500;
    line-height: inherit;
  }

  .formulari__grup input {
    color: #000000;
  }
  .formulari__grup input:focus {
    color: #0083ff;
  }
  .formulari__grup button {
    outline: none;
    background: #424656;
    width: 100%;
    border: 0;
    border-radius: 4px;
    padding: 12px 20px;
    color: #ffffff;
    font-family: inherit;
    font-size: inherit;
    font-weight: 500;
    line-height: inherit;
    text-transform: uppercase;
    cursor: pointer;
  }
  .formulari__grup button {
    background: #424656;
    color: #ffffff;
  }
  .formulari__grup .formulari__recordar {
    font-size: 12px;
    font-weight: 400;
    letter-spacing: 0;
    text-transform: none;
  }
  .formulari__grup .formulari__recordar input[type='checkbox'] {
    display: inline-block;
    width: auto;
    margin: 0 10px 0 0;
  }
  .formulari__grup .formulari__recuperar {
    color: #4285F4;
    font-size: 12px;
    text-decoration: none;
  }
  .formulari__panell {
    padding: 60px calc(4% + 60px) 60px;
    box-sizing: border-box;
  }
  .formulari__capcalera {
    margin: 0 0 40px;
  }
  .formulari__capcalera h1 {
    padding: 4px;
    color: #e93939;
    font-size: 24px;
    font-weight: 600;
    text-transform: uppercase;
  }
</style>
</head>
<body>
<div class="formulari" >
    <div class="formulari__panell">
        <div class="formulari__capcalera">
            <h1>Inici de Sessió</h1>
        </div>
        <div class="formulari__contingut">
            <form action="chat.php" method="POST">
                <div class="formulari__grup"><label for="username">Usuari</label><input type="text" id="nom" name="nom" required/></div>
                <div class="formulari__grup"><label for="password">Contrasenya</label><input type="password" id="contrasenya" name="contrasenya" required/></div>
                <div class="formulari__grup"><label class="formulari__recordar"></label><a class="formulari__recuperar" href="chat.php">Registrar-te</a></div>
                <div class="formulari__grup"><button type="submit" onclick="inicioSesion()">Iniciar Sessió</button></div>
            </form>
        </div>
    </div>
    <?php
    
    ?>
<script>
    function inicioSesion() {
      location.replace("chat.php")
    }
  </script>
</body>
</html>